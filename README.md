# traffic

## Description

Api testing tool

like postman or curl... but... limited in scope:

- send request with any verb (GET, POST, PUT, ...)
- add headers and body
- see the response

convenience:

- tools for form encoding
- save requests
- save history?

Not intrested in:

- auth stuff (just add the appropriate headers!)
- downloads, multi-part data, etc... if I don't need it, I don't want it
- cloud, login, anything like that..

## USAGE

Help

    traffic --help

Get. Protocol (http://) not required

    traffic postman-echo.com/get

Full response details

    traffic -v postman-echo.com/get

Force http (non-secure)

    traffic --http postman-echo.com/get

Add 'accept-language: en-US' header (required for some websites)

    traffic --en postman-echo.com/get

Add Query Parameters

    traffic \
    -q id 1234 \
    -q name "travis luke" \
    postman-echo.com/get

    => https://postman-echo.com/get?id=1234&name=travis+luke

Add Headers

    traffic \
    post \
    -h authorization "aa;sldkfja;sldkfj" \
    -h Content-type application/json \
    postman-echo.com/post

Add body (from file)

    traffic \
    post \
    -b path/to/file.json \
    postman-echo.com/post

## Useful urls

    https://jsonplaceholder.typicode.com/posts
    https://postman-echo.com/post
    https://postman-echo.com/get

## build

build

    go build -o build/ ./cmd/traffic/traffic.go

mac-m1

    GOOS=darwin GOARCH=arm64 go build -o build/macos-arm64/ cmd/traffic/traffic.go
