package app

import (
	"fmt"
)

func (a App) PrintHelp() {
	// Usage
	fmt.Print(`
USAGE:
    traffic [options] [<method>] <url>

OPTIONS:
    -h <key> <value>    Add HEADER to request with <value> for <key>
    -b <path>           Add BODY to request from file at <path>
    -q <key> <value>    Add QUERY PARAM to request with <value> for <key>
    --http              Set HTTP (non-secure) request. Default: HTTPS
    --en                Add "accept-language: en-US" header
    -v                  VERBOSE. print response details

ARGUMENTS:
    <method>            METHOD (optional) [get, post, put, delete]. Default 'get'
    <url>               URL (required). protocol (http:// ...) is ignored.

`)
}
