package app

import (
	"fmt"
	"os"
	"strings"

	"github.com/fatih/color"
	"gitlab.com/tluke12345/traffic/pkg/arguments"
	"gitlab.com/tluke12345/traffic/pkg/request"
)

// ----------------------------------------------------------------------------
// Specifications
// ----------------------------------------------------------------------------
var tokenizerSpec = arguments.TokenizerSpec{
	OneOptionArgs: []string{
		"-b",
	},
	TwoOptionArgs: []string{
		"-h",
		"-q",
	},
}

var commandSpec = arguments.CommandSpec{
	Flags: []string{
		"--help",
		"-h", "--header",
		"-q", "--query",
		"-b", "--body",
		"--http",
		"--en",
		"-v", "--verbose",
	},
}

// ----------------------------------------------------------------------------
// Data
// ----------------------------------------------------------------------------
type App struct {
	IsHelp    bool
	IsVerbose bool

	Request  request.Request
	Response request.Response
}

// ----------------------------------------------------------------------------
// Init
// ----------------------------------------------------------------------------
func Init(args []string) (*App, error) {
	a := &App{
		IsHelp:    false,
		IsVerbose: false,
	}

	req := &request.Request{
		Method:  "get",
		IsHTTPS: true,
		Headers: []request.KeyValue{},
		Query:   []request.KeyValue{},
	}

	tokens, err := arguments.Tokenize(args, tokenizerSpec)
	if err != nil {
		return a, err
	}

	cmd := arguments.ParseCommand(tokens, commandSpec)

	// Parse Flags
	for _, flagToken := range cmd.Flags {
		switch flagToken.Value {
		case "--help":
			a.IsHelp = true
		case "-v", "--verbose":
			a.IsVerbose = true
		case "-h", "--header":
			req.Headers = append(req.Headers, request.KeyValue{
				Key:   flagToken.Options[0],
				Value: flagToken.Options[1],
			})
		case "-q", "--query":
			req.Query = append(req.Query, request.KeyValue{
				Key:   flagToken.Options[0],
				Value: flagToken.Options[1],
			})
		case "-b", "--body":
			body, err := os.Open(flagToken.Options[0])
			if err != nil {
				return a, err
			}
			req.Body = body
		case "--http":
			req.IsHTTPS = false
		case "--en":
			req.Headers = append(req.Headers, request.KeyValue{
				Key:   "accept-language",
				Value: "en-US",
			})
		}
	}

	// Positional
	switch len(cmd.Positional) {
	case 0:
		return a, fmt.Errorf("Not enough arguments. at least 1 (url) is required")

	// 1 positional: url
	case 1:
		req.Url = urlForString(cmd.Positional[0].Value)

	// 2+ positional: method + url
	default:
		req.Method = cmd.Positional[0].Value
		req.Url = urlForString(cmd.Positional[1].Value)
	}

	a.Request = *req

	return a, nil
}

func urlForString(input string) string {
	url := input
	url = strings.TrimPrefix(url, "http://")
	url = strings.TrimPrefix(url, "https://")
	return url
}

// ----------------------------------------------------------------------------
// Print
// ----------------------------------------------------------------------------
func (a App) PrintVerbose() {
	variable := color.New(color.FgYellow).SprintFunc()

	fmt.Printf("isHelp %s\n", variable(a.IsHelp))
	fmt.Printf("isVerbose %s\n", variable(a.IsVerbose))

	color.HiMagenta("\nREQUEST")
	a.Request.PrintVerbose()

	color.HiMagenta("\nRESPONSE")
	a.Response.PrintVerbose()
}
