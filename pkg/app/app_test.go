package app

import "testing"

type UrlPattern struct {
	given    string
	expected string
}

func TestUrlForString(t *testing.T) {
	patterns := []UrlPattern{
		{given: "http://www.example.com", expected: "www.example.com"},
		{given: "https://www.example.com", expected: "www.example.com"},
		{given: "www.example.com", expected: "www.example.com"},
	}

	for _, pat := range patterns {
		actual := urlForString(pat.given)
		if actual != pat.expected {
			t.Fatalf("want '%s', got '%s'", pat.expected, actual)
		}
	}
}
