package arguments

import "slices"

// ----------------------------------------------------------------------------
// Data
// ----------------------------------------------------------------------------
type CommandSpec struct {
	Flags []string
}

type Command struct {
	Flags      []Token
	Positional []Token
}

// ----------------------------------------------------------------------------
// Parse
// ----------------------------------------------------------------------------
func ParseCommand(tokens []Token, spec CommandSpec) Command {
	cmd := Command{}
	for _, token := range tokens {
		if slices.Contains(spec.Flags, token.Value) {
			cmd.Flags = append(cmd.Flags, token)
		} else {
			cmd.Positional = append(cmd.Positional, token)
		}
	}
	return cmd
}
