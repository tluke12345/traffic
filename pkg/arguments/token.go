package arguments

import (
	"fmt"
	"slices"
)

// ----------------------------------------------------------------------------
// Data
// ----------------------------------------------------------------------------
type TokenizerSpec struct {
	OneOptionArgs []string
	TwoOptionArgs []string
}

type Token struct {
	Value   string
	Options []string
}

// ----------------------------------------------------------------------------
// INTERNAL
// ----------------------------------------------------------------------------
type state struct {
	values []string
}

func (s *state) next() string {
	value := s.values[0]
	s.values = s.values[1:]
	return value
}

// ----------------------------------------------------------------------------
// Tokenize
// ----------------------------------------------------------------------------
func Tokenize(args []string, spec TokenizerSpec) ([]Token, error) {
	s := state{values: args}
	tokens := []Token{}
	for len(s.values) > 0 {
		current := s.next()

		// One option args
		if slices.Contains(spec.OneOptionArgs, current) {
			if len(s.values) < 1 {
				return tokens, fmt.Errorf("%s argument requires one additional argument", current)
			}
			tokens = append(tokens, Token{
				Value:   current,
				Options: []string{s.next()},
			})
			continue
		}

		// Two option args
		if slices.Contains(spec.TwoOptionArgs, current) {
			if len(s.values) < 2 {
				return tokens, fmt.Errorf("%s argument requires two additional argument", current)
			}
			tokens = append(tokens, Token{
				Value:   current,
				Options: []string{s.next(), s.next()},
			})
			continue
		}

		// Default: flag (value only)
		tokens = append(tokens, Token{Value: current})
	}
	return tokens, nil
}
