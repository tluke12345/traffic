package request

import (
	"fmt"
	"io"
	"net/http"
	"strings"
)

func (r Request) Send() (Response, error) {
	client := &http.Client{}

	// Url
	var url string
	if r.IsHTTPS {
		url = fmt.Sprint("https://", r.Url)
	} else {
		url = fmt.Sprint("http://", r.Url)
	}

	// Method
	method := strings.ToUpper(r.Method)

	// Create Request
	req, err := http.NewRequest(method, url, r.Body)
	if err != nil {
		return Response{}, err
	}

	// Request Headers
	for _, pair := range r.Headers {
		req.Header.Add(pair.Key, pair.Value)
	}

	// Request Query parmas
	q := req.URL.Query()
	for _, pair := range r.Query {
		q.Add(pair.Key, pair.Value)
	}
	req.URL.RawQuery = q.Encode()

	// Send
	resp, err := client.Do(req)
	if err != nil {
		return Response{}, err
	}

	// Response Body
	defer resp.Body.Close()
	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return Response{}, err
	}

	// Response Headers
	headers := []KeyValue{}
	for key, values := range resp.Header {
		for _, value := range values {
			headers = append(headers, KeyValue{
				Key:   key,
				Value: value,
			})
		}
	}

	// TODO: how to determine any redirects and if url changed?

	return Response{
		Status:  resp.StatusCode,
		Body:    string(responseBody),
		Headers: headers,
	}, nil
}
