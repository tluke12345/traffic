package request

import (
	"fmt"

	"github.com/fatih/color"
)

type Response struct {
	Status  int        `json:"status"`
	Headers []KeyValue `json:"headers"`
	Body    string     `json:"body"`
}

func (r Response) PrintBody() {
	fmt.Println(r.Body)
}

func (r Response) PrintVerbose() {
	value := color.New(color.FgYellow).SprintfFunc()

	fmt.Printf("Status: %s\n", value("%d", r.Status))
	fmt.Printf("Headers:\n")
	for _, pair := range r.Headers {
		fmt.Printf("    %s: %s\n", pair.Key, value("%+v", pair.Value))
	}
	fmt.Printf("Body:%s\n", r.Body)
}
