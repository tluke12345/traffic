package request

import (
	"fmt"
	"io"

	"github.com/fatih/color"
)

type Request struct {
	Method  string
	Url     string
	IsHTTPS bool
	Headers []KeyValue
	Query   []KeyValue
	Body    io.Reader
}

func (r Request) PrintVerbose() {
	value := color.New(color.FgYellow).SprintfFunc()

	fmt.Printf("Url: %s\n", value("%s", r.Url))
	fmt.Printf("Method: %s\n", value("%s", r.Method))
	fmt.Printf("isHttps: %s\n", value("%s", r.IsHTTPS))
	fmt.Printf("Query:\n")
	for _, pair := range r.Query {
		fmt.Printf("    %s: %s\n", pair.Key, value("%+v", pair.Value))
	}
	fmt.Printf("Header:\n")
	for _, pair := range r.Headers {
		fmt.Printf("    %s: %s\n", pair.Key, value("%+v", pair.Value))
	}

	println("Body:")
	if r.Body == nil {
		return
	}

	if body, err := io.ReadAll(r.Body); err == nil {
		fmt.Println(body)
	}
}
