package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/tluke12345/traffic/pkg/app"
)

func main() {
	args := os.Args[1:]
	a, err := app.Init(args)
	if err != nil {
		fmt.Println(err)
		a.PrintHelp()
		return
	}

	if a.IsHelp {
		a.PrintHelp()
		return
	}

	res, err := a.Request.Send()
	if err != nil {
		log.Fatal(err)
	}

	a.Response = res
	if a.IsVerbose {
		a.PrintVerbose()
	} else {
		a.Response.PrintBody()
	}
}
